import { Schema, model } from 'mongoose'

const UserChema = new Schema({
    email: String,
    firstName: String,
    lastName: String
},{
    timestamps: true,
})

export default model('User', UserChema)